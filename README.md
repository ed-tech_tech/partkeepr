# Partkeepr Docker

Image of Partkeepr built using PHP7.1 and Nginx-fpm. Running the included `docker-compose.yml` creates a Partkeepr stack using MariaDB 10.0, with default database settings preconfigured in the Partkeepr setup page available at http://localhost:8080/setup/ (connect using hostname `database` in Partkeepr setup).

This image supports the `amd64`.

## Volumes

* `/partkeepr/app/config`: Partkeepr configuration folder.
* `/partkeepr/data`: Partkeepr data folder.
* `/partkeepr/web`: Partkeepr website root folder.

The authentication key generated during setup can be retrieved using:

```shell
docker exec partkeepr cat app/authkey.php
```
